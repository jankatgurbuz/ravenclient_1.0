from RavenLog import RavenLog_
import json
import time
import threading
import datetime
class ConfigHelper_():
	def __init__(self):
		self.availableIsActive = True
		self.jsonConfig={}
		self.isActive = False
		self.serverGroupId = 0
		self.serverType = ""
		self.ravenServerWebApiUrl = ""
		self.ravenServerExportCoreBackupUrl =""
		self.serviceList = {}
		self.mysqlUser=""
		self.mysqlPass=""
		self.mysqlHost=""
		self.platform=""
		self.importBackupCommand=""
		self.importLogCommand=""
		self.exportLogFile=""
		self.exportBackup={} 
		self.mySqlIsActive=True
		self.freshnessDataLog={}
		self.freshnessTagMaxSeconds=60
		self.takOkuFreshnessTolerance=0
		self.exportCoreBackUpControl= True

		
		self.ReadConfigFile()
	
#------------------------------------------------------------------
#  config icerisindeki jsonu alir , jsonConfig e atar.
#------------------------------------------------------------------
	
	def ReadConfigFile(self):
		try:
			file = open("Config.txt")
			readConfig=file.read()
			jsonConfig=json.loads(readConfig)
			self.jsonConfig=jsonConfig
			file.close()
		except:
			print("ReadConfigFile Exception : ",sys.exc_info()[1])
#------------------------------------------------------------------
#  degiskenlere jsondan gelen verileri atar
#------------------------------------------------------------------

	def LoadConfiguration(self):
		try:
			self.isActive = self.jsonConfig['isActive']
			self.serverGroupId = self.jsonConfig['serverGroupId']
			self.serverType = self.jsonConfig['serverType']
			self.ravenServerWebApiUrl = self.jsonConfig['ravenServerWebApiUrl']
			self.ravenServerExportCoreBackupUrl=self.jsonConfig['ravenServerExportCoreBackupUrl']
			self.serviceList=self.jsonConfig['serviceList']
			self.availableIsActive=self.jsonConfig['availableIsActive']
			self.platform=self.jsonConfig['platform']
			
			self.mysqlUser=self.jsonConfig['mysqlUser']
			self.mysqlPass=self.jsonConfig['mysqlPass']
			self.mysqlHost=self.jsonConfig['mysqlHost'] 
			
			self.importBackupCommand=self.jsonConfig['importBackupCommand']
			self.importBackupCommand=self.importBackupCommand.replace("[_0_]",self.mysqlUser)
			self.importBackupCommand=self.importBackupCommand.replace("[_1_]",self.mysqlPass)
			self.importLogCommand=self.jsonConfig['importLogCommand']
			self.importLogCommand=self.importLogCommand.replace("[_0_]",self.mysqlUser)
			self.importLogCommand=self.importLogCommand.replace("[_1_]",self.mysqlPass)
			
			self.exportLogFile=self.jsonConfig['exportLogFile']
			self.exportLogFile=self.exportLogFile.replace("[_0_]",self.mysqlUser)
			self.exportLogFile=self.exportLogFile.replace("[_1_]",self.mysqlPass)
			
			self.exportBackup=self.jsonConfig['exportBackup']
			for i in self.exportBackup:
				self.exportBackup[i]=self.exportBackup[i].replace("[_0_]",self.mysqlUser)
				self.exportBackup[i]=self.exportBackup[i].replace("[_1_]",self.mysqlPass)
			self.mySqlIsActive=self.jsonConfig['mySqlIsActive']
			
			self.freshnessDataLog=self.jsonConfig['freshnessDataLog']
			self.freshnessTagMaxSeconds=self.jsonConfig['freshnessTagMaxSeconds']
			self.takOkuFreshnessTolerance=self.jsonConfig['takOkuFreshnessTolerance']
		except:
			print("LoadConfiguration Exception : ",sys.exc_info()[1])
#------------------------------------------------------------------
# Config icerisindeki verileri degistirmek icin kullanilir
# key = json key alir
# values = yeni degeri alir 
#------------------------------------------------------------------

	def ChangeJsonValue(self,key,values):
		try:
			self.jsonConfig[key]=values
			jsonFile = open("Config.txt", "w")
			jsonFile.write(json.dumps(self.jsonConfig))
			jsonFile.close()
			self.ReadConfigFile()
			self.LoadConfiguration()
		except:
			print("ChangeJsonValue Exception : ",sys.exc_info()[1])
		
		
	