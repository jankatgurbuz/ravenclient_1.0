from RavenLog import RavenLog_
import requests
import json
import os
import time
import sys
import datetime
class Api_():

#------------------------------------------------------------------
# gelen url bilgisine gore get islemi 
#------------------------------------------------------------------	

	@staticmethod
	def get(url):
		try:
			r = requests.get(url)
			print(r.text)
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Get edilemedi .Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("Get Exception: ",sys.exc_info()[1])
		
#------------------------------------------------------------------
# gelen url bilgisine gore post islemi
# url = nereye post edilecek onun adresi
# json = post edilcek json 
#------------------------------------------------------------------	

	@staticmethod
	def post(url,json):
		try:
			headers = {'content-type': 'application/json'} # json formatinda oldugunu belirtiyoruz
			t = requests.post(url,data=json,headers=headers)
			print("post edildi")
			return t.text
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Post edilemedi.Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("Post Exception : ",sys.exc_info()[1])
		
#------------------------------------------------------------------
# gelen url bilgisine gore post islemi
# ExportCoreBackup icerisine ziplenmis olan dosyayi post eder
# url = nereye post edilecek onun adresi
#------------------------------------------------------------------	

	@staticmethod
	def postBackup(url):
		try:
			# post islemi
			
			fileobj = open('ExportCoreBackup/ExportCoreBackup.zip', 'rb')
			r = requests.post(url, data = {"mysubmit":"Go"}, files={"archive": ("ExportCoreBackup.zip", fileobj)})
			fileobj.close()
			print("Back up Post edildi")
			
			# dizinin icerisini boslatir
			
			for i in os.listdir("ExportCoreBackup"):
				os.remove("ExportCoreBackup/"+i)
		except :
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Backup post edilemedi.Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("Backup Post Exception : ",sys.exc_info()[1])
			
	
	@staticmethod
	def postLogfile(url):
		try:
			# post islemi
			
			fileobj = open('ExportLogFile/ExportLogFile.zip', 'rb')
			r = requests.post(url, data = {"mysubmit":"Go"}, files={"archive": ("ExportLogFile.zip", fileobj)})
			fileobj.close()
			print("Log Post edildi")
			
			# dizinin icerisini boslatir
			
			for i in os.listdir("ExportLogFile"):
				os.remove("ExportLogFile/"+i)
		except :
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Log post edilemedi.Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("Logfile Post Exception : ",sys.exc_info()[1])
		






