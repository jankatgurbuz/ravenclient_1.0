import psutil
import pysc
import sys
import MySQLdb
from RavenLog import RavenLog_
import datetime
import time
class UsageInformation_():
	@staticmethod
	def LinuxServicePsutil(serviceName):
			for proc in psutil.process_iter(attrs=['pid', 'name', 'username']):
				if (serviceName == proc.info['name']):
					procId = proc.info['pid']
					return psutil.Process(procId)
	@staticmethod
	def ServiceNameWindows(serviceName):
			liste = list(psutil.win_service_iter())
			for proc in liste:
				if(proc.name()==serviceName):
					break
			return proc.name()
					
					
#------------------------------------------------------------------
#  Cpu , ram , disk , network bilgilerini alir ve return eder. 
#------------------------------------------------------------------	
			
	@staticmethod
	def Information():
		try:
			
			# islemcinin % kac kullanildigini soyler
			
			process = psutil.cpu_percent()
			
			# memorynin % kac kullanildigini soyler
			
			memory = psutil.virtual_memory()
			memory=str(memory)
			list= memory.split(",")
			for i in list:
				if("percent" in i.lower()):
					memory=i.split("=")[1]
					
					# bu islemi yapmamim sebebi percent degeri sonda cikabiliyor ve  = tir ifadesinden sonrasini -
					# al diyorum. "  39.8) " gibi ifade donebiliyor bende ilk dort tanesini al dedim
					memory=memory[0]+memory[1]+memory[2]+memory[3]
					
			# diskin % kac kullanildigini soyler
			
			disk = psutil.disk_usage('/')
			disk=str(disk)
			list= disk.split(",")
			for i in list:
				if("percent" in i.lower()):
					disk=i.split("=")[1]
					disk=disk[0]+disk[1]+disk[2]+disk[3]
					
			network =  psutil.net_io_counters()
			return float(process) , float(memory) , float(disk) , network
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Sistem bilgileri alinamadi.Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("System Information Exception : ",sys.exc_info()[1])
			
		
#------------------------------------------------------------------
# istenilen servisin durum bilgisini verir. Servis durumunu post etmek icin. Linux icin calisir
# serviceName = servisin ismini alir.
#------------------------------------------------------------------		

	@staticmethod
	def LinuxServiceStatus(serviceName):
		try:
			for proc in psutil.process_iter(attrs=['pid', 'name', 'username']):
				#print("---------",proc.info['name'],"------------")
				if (serviceName == proc.info['name']):
					procId = proc.info['pid']
					return psutil.Process(procId).status()
		except:
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Linux servisleri bilgileri alinamadi.Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("Linux Service Status Exception: ",sys.exc_info()[1])
						
#------------------------------------------------------------------
# linux servisini baslatir.
# serviceName = servis ismini alir
#------------------------------------------------------------------	

	@staticmethod
	def LinuxServiceStart(serviceName):
		try:
			psutil_ = LinuxServicePsutil(serviceName)
			psutil_.resume()
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Linux servisleri baslatiliyor.",RavenLog_.INFORMATION)
		except :
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Linux servisleri baslatilamadi. Hata : " + str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("Linux Service Not Start Exception : ",sys.exc_info()[1])


#------------------------------------------------------------------
# linux servisini durdurur.
# serviceName = servis ismini alir
#------------------------------------------------------------------	

	@staticmethod
	def LinuxServiceStop(serviceName):
		try:
			psutil_ = LinuxServicePsutil(serviceName)
			psutil_.suspend()
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Linux servisleri durduruluyor.",RavenLog_.INFORMATION)
		except :
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Linux servisleri durdurulamadi."+ str(sys.exc_info()[1]) ,RavenLog_.ERROR)
			print("Linux Service Not Stop Exception : ",sys.exc_info()[1])
			
#------------------------------------------------------------------
# istenilen servisin durum bilgisini verir. Servis durumunu post etmek icin. windows icin calisir
# serviceName = servisin ismini alir.
#------------------------------------------------------------------	

	@staticmethod
	def WindowsServiceStatus(serviceName):
		try:
			liste = list(psutil.win_service_iter())
			for proc in liste:
				if(proc.name()==serviceName):
					break
			return proc.status()
		except:
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime
				("%d:%m:%Y")+" ")+"Windows servis bilgileri alinamadi. Hata: " + str(sys.exc_info()[1]) ,RavenLog_.ERROR)
			print("Windows Service Status Exception:: ",sys.exc_info()[1])
		

#------------------------------------------------------------------
# windows servisini durdurur.
# serviceName = servis ismini alir
#------------------------------------------------------------------	

	@staticmethod
	def WindowsServiceStop(serviceName):
		try:
			pysc.stop(serviceName)
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Windows servisleri durduruluyor" ,RavenLog_.INFORMATION)
		except :
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Windows servisleri durdurulamadi " + str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("Windows Service Not Stop Exception : ",sys.exc_info()[1])
			
#------------------------------------------------------------------
# windows servisini baslatir.
# serviceName = servis ismini alir
#------------------------------------------------------------------	

	@staticmethod
	def WindowsServiceStart(serviceName):
		try:
			pysc.start(serviceName)
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Windows servisleri baslatildi" ,RavenLog_.INFORMATION)
		except :
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Windows servisleri baslatilamadi" + str(sys.exc_info()[1]) ,RavenLog_.ERROR)
			print("Windows Service Not Start Exception : ",sys.exc_info()[1])
			
#------------------------------------------------------------------
# mysql servisini baslatir acar.Win icin ve linux icin calisir.
# configHelper = config icerigini getirir.
# mySqlStartOrStop = "START" verirsek baslar "STOP" verirsek durur.
# serviceName = servisin adini alir
#------------------------------------------------------------------	

	@staticmethod
	def MySqlServiceStartOrStop(configHelper,mySqlStartOrStop,serviceName):
		try:
			if(configHelper.platform=="windows"):
				if(mySqlStartOrStop=="START"):
					UsageInformation_.WindowsServiceStart(serviceName)
					RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
						"%d:%m:%Y")+" ")+"Mysql servisi baslatildi" ,RavenLog_.INFORMATION)
				elif(mySqlStartOrStop=="STOP"):
					UsageInformation_.WindowsServiceStop(serviceName)
					RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
						"%d:%m:%Y")+" ")+"Mysql servisi durduruldu" ,RavenLog_.INFORMATION)
			else:
				if(mySqlStartOrStop=="START"):
					UsageInformation_.LinuxServiceStart(serviceName)
					RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
						"%d:%m:%Y")+" ")+"Mysql servisi baslatildi" ,RavenLog_.INFORMATION)
				elif(mySqlStartOrStop=="STOP"):
					UsageInformation_.LinuxServiceStop(serviceName)
					RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
						"%d:%m:%Y")+" ")+"Mysql servisi durduruldu" ,RavenLog_.INFORMATION)
		except:
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"Mysql baslatma ya da durdurma hatasi. Hata : " + str(sys.exc_info()[1]) ,RavenLog_.ERROR)
			print("MySQL Service Not Start or Not Stop Exception : ",str(sys.exc_info()[1]))
			
#------------------------------------------------------------------
# Servis listesinde mysql var mi yok mu onu kontrol eder True,False ve servis ismini dondurur.
# configHelper = config icerigini getirir.
#------------------------------------------------------------------	

	@staticmethod
	def findMysqlService(configHelper):
		try:
			serviceName=""
			for services in configHelper.serviceList: 
				if("mysql" in configHelper.serviceList[services].lower()):
					serviceName=configHelper.serviceList[services]
			if(serviceName != ""):
				return True , serviceName
			else:
				return False ,serviceName
		except:
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+" Mysql bulma hatasi. Hata: " + str(sys.exc_info()[1]) ,RavenLog_.ERROR)
			print("MySQL Service Not Found Exception : ",sys.exc_info()[1])

#------------------------------------------------------------------
# Ornegin : Gelen cevapta servisin adi mysql olarak gelebilir.Configde servis listesinin -
# icerisinde ki mysql iceren ismi  bulur dondurur. Ornegin MySQL57 gibi. 
#------------------------------------------------------------------
# responseServiceName = gelen isim 
# configHelper = config icerigini getirir.
#------------------------------------------------------------------	
			
	@staticmethod
	def findServiceName(responseServiceName,configHelper):
		try:
			for i in configHelper.serviceList:
				if(responseServiceName in configHelper.serviceList[i].lower()):
					return configHelper.serviceList[i]
				else:
					return ""
			
		except:
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+" servis ismi bulunamadi  . Hata: " + str(sys.exc_info()[1]) ,RavenLog_.ERROR)
			print("findServiceName exception",sys.exc_info()[1])
	
#------------------------------------------------------------------
# ProcLogFreshnessDataLogTEST precedure : noktalog tablosunda logtime a gore -
# buyukten kucuge dogru siralamada-en buyuk degerin suan ki zaman ile arasındaki farki verir 
# eger results degiskeni buyukse freshnessLogMaxMinute degiskeninden sayacimizi artiririz,
# 5-4-3-2-1 tablo sayisina gore kontrol yapariz bunun sebebi bazi sunucularda log tablosunun az olmasi
# ornegin 5 tablo icin sayacimiz 3 ten buyukse control false olur yani sikinti durumu.
#-----------------------------------------------------------------
# configHelper = config icerigini getirir.
#------------------------------------------------------------------	

	@staticmethod
	def freshnessDataLog(configHelper):
		try:
			myLogTableName=configHelper.freshnessDataLog
			counter = 0
			control = True
			
			for i in myLogTableName:
				connection = MySQLdb.connect(host=configHelper.mysqlHost,
									 user=configHelper.mysqlUser,
									 password=configHelper.mysqlPass,db = "kbindb")
				with connection.cursor() as cursor:
					
					cursor.execute("CALL `ProcLogFreshnessDataLogTEST`('"+myLogTableName[i]+"')")
					results = cursor.fetchone()
					if(int(results[0]) > 3  ): # !!!!!!! freshnessLogMaxMinute diye bir degisken olusturacagim
						counter = counter + 1
						
			if(len(myLogTableName)==5):
				if(counter>3):
					control = False
			elif(len(myLogTableName)==4):
				if(counter>2):
					control = False
			elif(len(myLogTableName)==3):
				if(counter>2):
					control = False
			elif(len(myLogTableName)==2):
				if(counter>1):
					control = False
			elif(len(myLogTableName)==1):
				if(counter>1):
					control = False
			return control
		except:
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+" freshnessDataLog hatasi . Hata: " + str(sys.exc_info()[1]) ,RavenLog_.ERROR)
			print("FreshnessDataLog Exception",sys.exc_info()[1])

#------------------------------------------------------------------
# procFreshnessTagReadInSecondsTEST precedure : tagoku_ tablosundaki - 
# güncellemeleri suan ki zaman ile farkinin ortalamasini verir.
# bazi noktalarin haberlesmesi olmadigi icin bir tolerans degeri verdik
# bunu procedure e parametre olarak gonderiyoruz. Bu parametre bize -
# config icerisinden geliyor
#-----------------------------------------------------------------
# configHelper = config icerigini getirir.
#------------------------------------------------------------------	
			
	@staticmethod
	def freshnessTagReadInSeconds(configHelper):
		try:
			connection = MySQLdb.connect(host=configHelper.mysqlHost,
								user=configHelper.mysqlUser,
								password=configHelper.mysqlPass,db = "kbindb")
			with connection.cursor() as cursor:
				cursor.execute("CALL `procFreshnessTagReadInSecondsTEST`('"+str(configHelper.takOkuFreshnessTolerance)+"')")
				results = cursor.fetchone()
				print(results[0])
				if(results[0]<configHelper.freshnessTagMaxSeconds):
					return True
				else:
					return False
		except:
			RavenLog_.RavenLogWrite(str(time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+" freshnessTagReadInSeconds . Hata: " + str(sys.exc_info()[1]) ,RavenLog_.ERROR)
			print(" FreshnessTagReadInSeconds Exception",sys.exc_info()[1])
		
			
					
			
			

		
	
	
		