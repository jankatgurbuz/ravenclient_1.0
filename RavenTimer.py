from threading import Timer,Thread,Event

class Timer_():
	def __init__(self,time,function,now):
		self.time=time
		self.function=function
		self.now=now
		if(now == True):
			self.thread=Timer(self.time,self.handle_function)
		else:
			self.thread=Timer(0,self.handle_function)
	def handle_function(self):
		self.function()
		self.thread=Timer(self.time,self.handle_function)
		self.thread.start()
	def start(self):
		self.thread.start()
	def cencel(self):
		self.thread.cancel()