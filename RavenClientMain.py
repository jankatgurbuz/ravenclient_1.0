from ConfigHelper import ConfigHelper_
from RavenTimer import Timer_
from GetUsageInformation import UsageInformation_
from RavenRestApi import Api_
from RavenCoreBackupAndLogFile import BackupAndLogFile_
from RavenLog import RavenLog_
import json
import psutil
import sys
import time
import threading

configHelper = ConfigHelper_()
configHelper.LoadConfiguration()

#coreBackupTimer=None

#------------------------------------------------------------------
# Gelen cevaplar islenir 
# service = gelen servis listesini alir.
# isActive = aktif mi degil mi  alir.
# coreBackupFile = coreBackup var mi yok mu alir.
# logFile = log var mi yok mu alir.
#------------------------------------------------------------------

def responseEvent(service,isActive,coreBackupFile,logFile):
	try:
		#açılıp kapanacak servis var mi ? 
		print("Cevaplar Geldi")
		if(service):
			print("1 Gelen Servis var mi")
			manageService(service)
#--------------------------------------------------------------------------------		

		#aktif grup sen misin?
		
		if(isActive==configHelper.serverGroupId):
			configHelper.ChangeJsonValue("isActive",isActive)
			if(configHelper.availableIsActive == False):
				print("butun servisler calistiriliyor")
				runAllService(configHelper.serviceList)
				configHelper.ChangeJsonValue(key="availableIsActive",values = True)	
				
		else:
			configHelper.ChangeJsonValue("isActive",isActive)
			if(configHelper.availableIsActive == True):
				if(configHelper.serverType == "slave"):
					if(configHelper.mySqlIsActive == True ):
						
						print("Log aliniyor")
						BackupAndLogFile_.exportLog(configHelper)
						#BackupAndLogFile_.sqlRenameDB(configHelper)  # !!!!!!!!! UNUTMA ACMAYI
						Api_.postLogfile("""http://10.10.20.124:8083/Api/values""")
						print("Log aliniyor")
						stopAllService(configHelper.serviceList)
						configHelper.ChangeJsonValue(key="availableIsActive",values = False)	
						
#-------------------------------------------------------------------------------
				
		#CoreBackup var mi ?
		
		if(coreBackupFile != ""): # win(mysql yok) ve linux(mysql var) olarak iki sunucu olursa ikisine aynı cevap gelecek ama
			if(configHelper.serverType == "slave"): 									   # mysql sunucusu corebackup yapacak
				control,serviceName=UsageInformation_.findMysqlService(configHelper)
				if(control== True):	# mysql var ise 
					print("core backup import ediliyor")
					if(configHelper.mySqlIsActive == False): # mysql var ama suan kapali
						UsageInformation_.MySqlServiceStartOrStop(configHelper,"START",serviceName)
						BackupAndLogFile_.BackupDownloadAndUnZip(coreBackupFile) # backup indiriyor
						BackupAndLogFile_.BackupFileImport(configHelper)
						UsageInformation_.MySqlServiceStartOrStop(configHelper,"STOP",serviceName)
					else: # mysql var ve acik
						BackupAndLogFile_.BackupDownloadAndUnZip(coreBackupFile) # backup indiriyor
						BackupAndLogFile_.BackupFileImport(configHelper)
						
#--------------------------------------------------------------------------------

		#Log var mi ?
		
		if(logFile!=""):
			#(isActive==configHelper.serverGroupId) sebebi zaten pasif olan bir master log almaz ama oldu bi hata oldu bilgi geldi
			# genede log alamaz. 
			if((configHelper.serverType == "master") and (isActive==configHelper.serverGroupId)):
				control,serverName=UsageInformation_.findMysqlService(configHelper)
				if(control== True):	# mysql var ise 
					print("log indiriliyor import ediliyor")
					BackupAndLogFile_.LogDownloadAndUnZip(logFile) 
					BackupAndLogFile_.LogFileImport(configHelper)
	except:
		print("responseEvent Exception : ",sys.exc_info()[1])
	
											
#------------------------------------------------------------------
# Config icerisinde ki butun servisleri balatir.
# configHelperServiceList = config dosyasinin icerisindeki servislerin listesini alir.
#------------------------------------------------------------------
	
def runAllService(configHelperServiceList):
	try:
		for i in configHelperServiceList:
			if(i != ""):
				if(configHelper.platform == "windows"):
					UsageInformation_.WindowsServiceStart(configHelperServiceList[i])
				else:
					UsageInformation_.LinuxServiceStart(configHelperServiceList[i])
				# sunucuda mysql var ise config dosyasındaki mySqlIsActive true yapar
				
		control,serviceName=UsageInformation_.findMysqlService(configHelper)
		if(control == True):
			configHelper.ChangeJsonValue("mySqlIsActive",True)
	except:
		print("runAllService Exception : ",sys.exc_info()[1])

#------------------------------------------------------------------
# Config icerisinde ki butun servisleri durdurur.
# configHelperServiceList = config dosyasinin icerisindeki servislerin listesini alir.
#------------------------------------------------------------------
				
def stopAllService(configHelperServiceList):

	try:
		for i in configHelperServiceList:
			if(i != ""):
				if(configHelper.platform == "windows"):
					UsageInformation_.WindowsServiceStop(configHelperServiceList[i])
				else:
					UsageInformation_.LinuxServiceStop(configHelperServiceList[i])
				# sunucuda mysql var ise config dosyasındaki mySqlIsActive false yapar
		control,serviceName=UsageInformation_.findMysqlService(configHelper)
		if(control == True):
			configHelper.ChangeJsonValue("mySqlIsActive",False)
	except:
		print("stopAllService Exception : ",sys.exc_info()[1])
				

#------------------------------------------------------------------
# Gelen cevapta istenilen servisleri calisirir.
# service = servis listesini alir.
#------------------------------------------------------------------

def manageService(service):

	try:
		print("\n1\tMySQL var mi ? ")
		mysqlControl,mySqlName = UsageInformation_.findMysqlService(configHelper) #mysql var mi config icerisinde ? 
		for i in service:
			if(i != ""):
				
				# gelen cevaba göre servisin config icerisindeki ismine bakar ve return eder
				
				configServiceName = UsageInformation_.findServiceName(i,configHelper) 
				
				# 2 master sunucumuz olsun biri linux biri win.Ikisine de ayni cevap gidecek ve birisinde ki servis digerinde olmayabilir
				# bu yuzden config dosyasinda servisi bulamazsa bos return eder buda servisi baslatmaz 
				
				if(configServiceName != ""):
				
					if(service[i] == "START"): 
						print(configServiceName+" 1----------"+" "+service[i]+"  "+configHelper.platform )
						print("\n1\t"+i+" baslatiliyor. (gelen cevaba karsilik)")
						if(configHelper.platform == "windows"):
							
							UsageInformation_.WindowsServiceStart(configServiceName)
						else:
							UsageInformation_.LinuxServiceStart(configServiceName)
						if(mysqlControl == True):
							print("\n1\tmysql  baslatiliyor. (gelen cevaba karsilik)")
							configHelper.ChangeJsonValue(key="mySqlIsActive",values = True)
							
					if(service[i] == "STOP"):
						print("\n1\t"+i+" durduruluyor. (gelen cevaba karsilik)")
						if(configHelper.platform == "windows"):
							UsageInformation_.WindowsServiceStop(configServiceName)
						else:
							UsageInformation_.LinuxServiceStop(configServiceName)
						if(mysqlControl == True):
							print("\n1\tmysql  durduruluyor. (gelen cevaba karsilik)")
							configHelper.ChangeJsonValue(key="mySqlIsActive",values = False)
	except:
		print("manageService Exception : ",sys.exc_info()[1])
	
					
#------------------------------------------------------------------
# Cpu,ram,disk,network bilgisini ve  servis durumlari listesini post eder
#------------------------------------------------------------------

def StatusPostTimer():
	try:
		
		processInformation,memoryInformation,diskInformation,networkInformation = UsageInformation_.Information()
		
		# servis durunlarini alir 
		
		myservice = {}
		if(configHelper.platform == "windows"):
			for services in configHelper.serviceList: 
				myservice[configHelper.serviceList[services]]=UsageInformation_.WindowsServiceStatus(configHelper.serviceList[services])
		else:
			for services in configHelper.serviceList:
				myservice[configHelper.serviceList[services]]=UsageInformation_.LinuxServiceStatus(configHelper.serviceList[services])
				
		# json olusturur
		
		jsonStatusPost = {}
		jsonStatusPost['processInformation']=str(processInformation)
		jsonStatusPost['memoryInformation']=str(memoryInformation)
		jsonStatusPost['diskInformation']=str(diskInformation)
		jsonStatusPost['networkInformation']=str(networkInformation)
		jsonStatusPost['serviceList'] = myservice
		
		# sunucuda mysql var ise freshness bilgilerini aliyor
		
		if(configHelper.mySqlIsActive == True):
			jsonStatusPost['logFreshness'] = UsageInformation_.freshnessDataLog(configHelper)
			jsonStatusPost['freshnessTagReadInSeconds'] = UsageInformation_.freshnessTagReadInSeconds(configHelper)
			
		json_data=json.dumps(jsonStatusPost)
		
		# bilgileri post eder
		
		response = Api_.post(configHelper.ravenServerWebApiUrl,json_data)
		responseJson=json.loads(response)
		
		#gelen cevabi responseEvent metoduna gonderir
		
		responseEvent(responseJson['service'],responseJson['isActive'],responseJson['CoreBackupFile'],responseJson['LogFile'])
	except:
		print("StatusPostTimer Exception : ",sys.exc_info()[1])

#------------------------------------------------------------------
# CoreBackup alip zipler , zip olarak post eder
#------------------------------------------------------------------

def ExportCoreBackup():

	try:
		if(configHelper.isActive == configHelper.serverGroupId):
			if(configHelper.mySqlIsActive == True):
				
				BackupAndLogFile_.exportBackup(configHelper)
				url = configHelper.ravenServerExportCoreBackupUrl
				Api_.postBackup(url)
	except:
		print(" ExportCoreBackup Exception: ",sys.exc_info()[1])
	
		
#------------------------------------------------------------------
# Main -->
#------------------------------------------------------------------
def log():
	RavenLog_.changeFileName(str( time.strftime("%H_%M_%S")+"_"+time.strftime("%d_%m_%Y")+" "))
if __name__ == "__main__":

	#StatusPostTimer()
	#statusPostTimer = Timer_(60,StatusPostTimer)
	#statusPostTimer.start()
	
	t = Timer_(180,log,False)
	t.start()
	
	exprtcore= Timer_(15,ExportCoreBackup,True)
	exprtcore.start()

	while True:
		
	
		StatusPostTimer()
		
		#exportCoreBackUpThread = threading.Thread(target=ExportCoreBackup)
		time.sleep(5)
		# islemci ram disk network durumlarini alir
	
	
		
	#UsageInformation_.freshnessTagReadInSeconds(configHelper)
	#print(UsageInformation_.freshnessTagReadInSeconds(configHelper))
	#UsageInformation_.Information()
	
	
	
	
	
	
	
	
	




