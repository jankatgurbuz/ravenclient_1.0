from RavenLog import RavenLog_
from tqdm import tqdm
import requests
import zipfile
import MySQLdb
import time
import warnings
from subprocess import check_output
import os
import datetime
class BackupAndLogFile_():

#------------------------------------------------------------------
# Gelen url e göre indirir zipten cikarir
# url= indirilecek url bilgisi
#------------------------------------------------------------------		

	@staticmethod
	def BackupDownloadAndUnZip(url):
		try:
			# indirme işlemi
			
			response = requests.get(url, stream=True)
			fileName="""ImportCoreBackUp/sql.zip"""
			with open(fileName, "wb") as handle:
				for data in tqdm(response.iter_content()):
					handle.write(data)
					
			# zipten cikartma islemi
			
			zip_ref = zipfile.ZipFile(fileName, 'r')
			
			# ImportCoreBackUp dizininin icerisine yazar
			
			zip_ref.extractall('ImportCoreBackUp') 
			zip_ref.close()
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"backup indirilemedi ya da zipten cikarilamadi.Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("BackupDownloadAndUnZip Exception: ",sys.exc_info()[1])
		
#------------------------------------------------------------------
# .sql dosyasini ice aktarir.
# configHelper = config icerigine ulasiriz.
#------------------------------------------------------------------	

	@staticmethod
	def BackupFileImport(configHelper):
		try:
			temporary=""
			
			# ImportCoreBackUp dizinin icerisinde .sql ile biteni bulur
			
			for i in os.listdir("ImportCoreBackUp"):
				if(".sql" in i):
				
					# configHelper.importBackupCommand gelen komutun sonuna .sql ile biten 
					# dosya adini ekler boylece indirilen dosyanin adi ne olursa olsun
					# .sql ile bitiyorsa onu import eder. 
					
					temporary=configHelper.importBackupCommand+"\\"+i+"\""
					check_output(temporary,shell = True)
					
				# import isleminden sonra dizini bosaltir.
				
				os.remove("ImportCoreBackUp/"+i)
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+" backup import edilemedi .Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("BackupFileImport Exception: ",sys.exc_info()[1])

	
#------------------------------------------------------------------
# Log islemleri bitince devam et !!!!!!
# 
#------------------------------------------------------------------		
	
	@staticmethod
	def LogDownloadAndUnZip(url):
		try:
			# indirme işlemi
			
			response = requests.get(url, stream=True)
			fileName="""ImportLogFile/sql.zip"""
			with open(fileName, "wb") as handle:
				for data in tqdm(response.iter_content()):
					handle.write(data)
					
			# zipten cikartma islemi
			
			zip_ref = zipfile.ZipFile(fileName, 'r')
			zip_ref.extractall('ImportLogFile') # bulundugu konuma yaziyor 
			zip_ref.close()
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"log indirilemedi ya da zipten cikarilamadi.Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("LogDownloadAndUnZip Exception: ",sys.exc_info()[1])
		
		
	@staticmethod
	def LogFileImport(configHelper):
		try:
			temporary=""
			for i in os.listdir("ImportLogFile"):
				if(".sql" in i):
					temporary=configHelper.importLogCommand+"\\"+i+"\""
					check_output(temporary,shell = True)
				os.remove("ImportLogFile/"+i)
			print(temporary)
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"log import edilemedi: "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("LogFileImport Exception: ",sys.exc_info()[1])
		
#------------------------------------------------------------------
# config icerisine girdiğimiz comutlari calistirip backup aliriz 
# iki komut oldugu icin configHelper.exportBackup bir listedir
# bir dongu ile icerigini teker teker dizine atar ve zipleriz
# zipli klasor icersinde 2 tane .sql dosyasi olmali
# configHelper = config icerigine ulasiriz.
#------------------------------------------------------------------
	@staticmethod
	def exportBackup(configHelper):
		try:
			for i in configHelper.exportBackup:
				check_output(configHelper.exportBackup[i],shell = True)
			
			# 2 tane .sql dosyasini zipleme islemi
			
			with zipfile.ZipFile('ExportCoreBackup/ExportCoreBackup.zip', mode='w') as zf:
					zf.write('ExportCoreBackup/Backup1.sql', compress_type = zipfile.ZIP_DEFLATED)
					zf.write('ExportCoreBackup/Backup2.sql', compress_type = zipfile.ZIP_DEFLATED)
					zf.close()
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"backup export edilemedi.Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("exportBackup Exception: ",sys.exc_info()[1])
					
#------------------------------------------------------------------
# noktalog tablosundan loglari zipleriz.
#------------------------------------------------------------------				
	
	@staticmethod
	def exportLog(configHelper):
		try:
			check_output(configHelper.exportLogFile,shell = True)
			with zipfile.ZipFile('ExportLogFile/ExportLogFile.zip', mode='w') as zf:
					zf.write('ExportLogFile/log', compress_type = zipfile.ZIP_DEFLATED)
					zf.close()
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"log export edilemedi.Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("exportLog Exception: ",sys.exc_info()[1])
			
#------------------------------------------------------------------
# noktalog tablosundan farkli bir tablo olusturur, noktalog icerigini bu tabloya atariz.
# herhangi bir sikinti ol
#------------------------------------------------------------------		
			
	
	@staticmethod
	def sqlRenameDB(configHelper):
		try:
			connection = MySQLdb.connect(host=configHelper.mysqlHost,
								 user=configHelper.mysqlUser,
								 password=configHelper.mysqlPass)
			with connection.cursor() as cursor:
				db="noktalog"+str( time.strftime("%H_%M_%S")+"_"+time.strftime("%d_%m_%Y")+" ")
				createDB= "CREATE DATABASE "+db
				print(createDB)
				cursor.execute(createDB)
			sorgu="RENAME TABLE "
			with connection.cursor() as cursor: 
				sql = "select TABLE_NAME from  INFORMATION_SCHEMA.TABLES WHERE table_schema= 'noktalog'"
				cursor.execute(sql)
				for i in cursor:
					sorgu += " noktalog." +str(i[0])+" TO " +db+"."+str(i[0])+"," 
			sorgu=sorgu[:-1]
			sorgu= sorgu +";"
			with connection.cursor() as cursor:
				print(time.strftime("%c"))
				print(sorgu)
				cursor.execute(sorgu)
			with connection.cursor() as cursor:
				cursor.execute("DROP DATABASE noktalog")
		except:
			RavenLog_.RavenLogWrite(str( time.strftime("%H.%M.%S")+"---"+time.strftime(
				"%d:%m:%Y")+" ")+"noktalog tablosunun ismi degistirilemedi .Hata : "+str(sys.exc_info()[1]),RavenLog_.ERROR)
			print("sqlRenameDB Exception: ",sys.exc_info()[1])
		
		
		
		
		
		
		
		
				
				
			
								 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
							 
		
	
